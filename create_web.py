#!/usr/bin/env python3

# Create web pages.
# JBC. August 2007.

import sys
import os


def main():
    write_html('index.html', 'My Personal Page', 'home', 'mind_map')
    write_html('about.html', 'About', 'about', 'mind_map_mini')
    write_html('curriculum.html', 'Curriculum Vitae', 'curriculum',
               'mind_map_mini')
    write_html('contact.html', 'Contact', 'contact', 'mind_map_mini')

    write_html('notyet.html', 'Not There Yet', 'notyet', 'mind_map_mini')

    create_emtpy_links()


def create_emtpy_links():
    # The nodes are specified in the first part of mind_map.txt
    nodes = open('source/mind_map.txt').read().split('\n\n')[0]
    for node in nodes.replace(' ', '_').splitlines():
        if '=' not in node and not os.path.exists(node + '.html'):
            os.system('ln -s notyet.html %s.html' % node)


def write_html(filename, title, name, mmap):
    if len(sys.argv) == 1:
        check = True
    elif len(sys.argv) == 2 and sys.argv[1] == '--yes':
        check = False
    else:
        sys.exit('usage: %s [--yes]' % sys.argv[0])

    if check:
        if os.path.exists(filename):
            answer = input(filename + ' already exists. Continue? [y/n] ')
            if not answer.lower().startswith('y'):
                sys.exit()

    fout = open(filename, 'wt')

    fout.write("""\
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>%s</title>
<meta name="keywords" content="personal jordi">
<meta name="description" content="jordi burguet-castell personal page">
<link href="metamagical.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="images/favicon.png">
</head>

<body>
<!-- start header -->
<div id="header">
<div id="logo">
<h1>Jordi Burguet-Castell</h1>
<h2>%s</h2>
</div>
<div id="menu">
<ul>
  <li><a href="index.html">home</a></li>
  <li><a href="about.html">about</a></li>
  <li><a href="curriculum.html">curriculum</a></li>
  <li><a href="contact.html">contact</a></li>
</ul>
</div>
</div>
<!-- end header -->

<!-- start page -->
<div id="page">
""" % (title, name))

    fout.write(fill_content(name, mmap))

    fout.write("""
</div>
<!-- end page -->
<div id="footer">
<p id="legal">Copyleft 2007 &ndash; 2021</p>
<!-- Design based on "newave" at "Free CSS Templates" (http://www.freecsstemplates.org/) -->
</div>

</body>
</html>
""")


def fill_content(name, mmap):
    "Return the content of source/<name>.content with some substitutions"
    content = ''
    for line in open('source/%s.content' % name):
        if '<!-- SUBSTITUTE-MAP -->' in line:
            for line in open(mmap + '.map'):
                content += line
        else:
            content += line

    return content



if __name__ == '__main__':
    main()
