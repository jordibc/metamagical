all:
	./create_graph.py
	./create_web.py --yes

upload:
	rsync -azv -e ssh *.html *.css *.png *.map images bb:/var/www/metamagical.org

clean:
	rm -f *.html *.png *.map *.dot
