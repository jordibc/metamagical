#!/usr/bin/env python3

# Create clickable graphs as a "mind-map", to be used in web pages.
# JBC. August 2007.

import os
import re
from pydot import Dot, Node, Edge


def main():
    create_map('source/mind_map.txt', map_name='mind_map',
               rankdir='LR', scale=80)

    create_map('source/mind_map.txt', map_name='mind_map_mini',
               rankdir='TB', scale=50)



def create_map(description_file, map_name='mind_map',
               rankdir='LR', scale=100, fontname='Alba Super', fontsize=16):
    "Create 'map_name'.png and 'map_name'.map files, client-side html maps"
    check_font(fontname)

    g = Dot()
    g.set_name(map_name)
    g.set_type('graph')  # no arrows in the links
    g.set_rankdir(rankdir)  # vertical, landscape, etc
    g.set_bgcolor('transparent')
    g.set_splines(True)
    g.set_overlap(False)
    g.set_ranksep(0.1)
    g.set_nodesep(0.1)

    # Read and fill the map, using my peculiar format.
    with open(description_file) as f:
        # Nodes.
        while True:
            line = f.readline().rstrip()
            if not line:
                break
            if line.startswith('#'):
                continue  # that was a comment, ignore it
            pos = line.find('=')
            if pos != -1:
                name = line[:pos]
                url = line[pos+1:]
            else:
                name = line
                url = re.sub('&', 'and', re.sub(' ', '_', name)) + '.html'
            n = Node(name, URL=url, fillcolor=name_color(name), style='filled',
                     fontsize=fontsize, fontname=fontname)
            g.add_node(n)

        # Links
        for line in f.readlines():
            if line.startswith('#'):
                continue  # that was a comment, ignore it
            edges = line.rstrip().split(',')
            g.add_edge(Edge(edges[0], edges[1]))

    # Write the files

    # We want to do
    #   g.write_png('%s.png' % map_name, prog='dot')  # write image
    # but it doesn't find the fonts in ~/.local/share/fonts, so instead:
    g.write_dot('%s.dot' % map_name)
    os.system('dot -Tpng %s.dot > %s.png' % (map_name, map_name))

    g.write_cmapx('%s.map' % map_name, prog='dot')  # write web clickable map

    if scale != 100:
        create_mini_map(map_name=map_name, scale=scale)


def create_mini_map(map_name='mind_map_mini', scale=40):
    os.system('cp %s.png tmp.png' % map_name)
    os.system('convert tmp.png -resize %d%% %s.png' % (scale, map_name))
    os.system('rm tmp.png')

    os.system('cp %s.map tmp.map' % map_name)
    with open('%s.map' % map_name, 'wt') as fout:
        for line in open('tmp.map'):
            m = re.match('(.*coords=")(.*?)(".*)', line)
            if not m:
                fout.write(line)
                continue

            fout.write(m.groups()[0])

            coords = [int(x) for x in m.groups()[1].replace(' ', '').split(',')]
            fout.write(','.join('%d' % (scale * x // 100) for x in coords))

            fout.write(m.groups()[2] + '\n')
    os.system('rm tmp.map')


def name_color(name):
    "Return a rgba color derived from a name"
    # Useful to assign "random" colors to the nodes in the graph.
    factors = [2**i % 11 for i in range(11)]
    index = sum(factors[i % 11] * ord(x) for i, x in enumerate(name)) % 18
    index1 = index % 3
    index0 = (index // 9) % 2
    index01 = (index // 6) % 3
    pos1 = index1
    pos0 = [x for x in range(3) if x != pos1][index0]
    pos01 = [x for x in range(3) if x not in (pos1, pos0)][0]
    values = [0, 0, 0]
    values[pos1] = 255
    values[pos0] = 50
    values[pos01] = index01 * 127
    return '#%02x%02x%02x80' % tuple(values)


def check_font(fontname):
    "Print a warning if fontname is not known"
    ret = os.system('fc-list -q "%s"' % fontname)
    if ret != 0:
        print('Warning: cannot find font "%s"' % fontname)



if __name__ == '__main__':
    main()
